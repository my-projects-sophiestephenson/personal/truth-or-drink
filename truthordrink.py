##
## truthordrink.py
## sophie stephenson
## october 2020
##

import sys
import csv
from random import choice

# make sure inputs are fine
if not len(sys.argv) == 2:
    exit("Usage: truthordrink.py [\"extra dirty\" or \"on the rocks\"]")

file_dict = {"on the rocks": "on_the_rocks.csv", "extra dirty": "extra_dirty.csv"}

if sys.argv[1] not in file_dict.keys():
    exit("Please enter a valid game type (\"extra dirty\" or \"on the rocks\").")

# gather question list from the appropriate csv file
questions = []
with open(file_dict[sys.argv[1]]) as f:
    reader = csv.reader(f, delimiter=",")
    for row in reader:
        questions.append((row[0], row[1]))

# game play: 
while (len(questions) > 0):
    input("--------- Hit enter to pick a card. ---------")
    q = choice(questions)
    print("A)", q[0], "\nB)", q[1], "\n")
    questions.remove(q)
    if (len(questions) == 5):
        print("Only 5 cards left!\n")
    if (len(questions) == 0):
        print("That's the game!")

