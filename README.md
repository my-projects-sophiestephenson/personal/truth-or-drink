# Truth or Drink: The Command Line Version

A bit ago, I decided to buy the [Truth or Drink](https://www.playtruthordrink.com) game I had seen in so many Youtube Videos (like [this one](https://www.youtube.com/watch?v=WoGY_73GVGA&list=PLJic7bfGlo3qgrRJsCm5RbtOxZ8Q5WSr_&index=50).) I opted for the $7 PDF version instead of the $35 set of physical cards, but when I tried to actually play, it was a bit artificial because players could choose a card instead of it being random.

Enter this project! A simple way to add randomness to the PDF version of the game. Here's what I did:

1. I copied over the card decks into CSV files with two columns, one for each question on the card. To do this, I made a Google Sheet in that format and exported it as a CSV file. It was tedious, but got the job done. (I did not include the files in this repository, so that I don't inadvertantly cause Truth or Drink to lose money.)
2. I wrote `truthordrink.py` to facilitate game play. The file loads up whichever CSV a player wants to use (I only have the two I use, which are Extra Dirty and On the Rocks), then randomly "picks a card" from the set of questions until there are none left. And that's it!

To use (once you have your CSV files), simply enter this into the command line:

```python3 truthordrink.py ['extra dirty' or 'on the rocks']```